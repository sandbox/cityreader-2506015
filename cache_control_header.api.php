<?php

/**
 * Define paths that should be ignored by cache_control_header module.
 * So cache-control header of those paths will be untouched.
 */
function hook_cache_control_header_ignore_path() {
  return 'node/add/*
node/*/edit';
}
