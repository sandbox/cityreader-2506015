<?php
/**
 * @file
 * Provide admin config page for cache_control_header.
 */

/**
 * Page callback for cache_control_header admin config form.
 */
function cache_control_header_admin_config($form, &$form_state) {
  drupal_set_title(t('Cache-control header configuration'));

  $settings = cache_control_header_settings();

  $options = user_roles($membersonly = TRUE);
  $options = array_diff($options, array('authenticated user'));

  $form['ignore_path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ignore paths'),
    '#description' => 'Cache-control header will remain no-cache value in the following paths.',
    '#collapsible' => TRUE,
  );

  $default_paths = cache_control_header_get_ignore_paths();

  $form['ignore_path']['default_path'] = array(
    '#type' => 'textarea',
    '#title' => t('Admin paths'),
    '#description' => t('This is read-only field with a list of paths added by implementing hook_cache_control_header_ignore_path().'),
    '#value' => $default_paths,
    '#disabled' => TRUE,
  );

  $form['ignore_path']['custom_path'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom paths'),
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
    '#default_value' => $settings['custom_path'],
  );

  $form['ignore_roles'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ignore user roles'),
    '#description' => t('Cache-control header will remain no-cache value in the following user roles.'),
    '#collapsible' => TRUE,
  );

  $form['ignore_roles']['roles'] = array(
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => $settings['roles'],
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));

  return $form;
}

/**
 * Submit handler for cache_control_header admin config form.
 */
function cache_control_header_admin_config_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  $settings = array();
  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value)) {
      $value = array_filter($value);
    }
    $settings[$key] = $value;
  }

  $settings['page_cache_maximum_age'] = variable_get('page_cache_maximum_age', 0);

  variable_set('cache_control_header_settings', $settings);
  drupal_set_message(t('The configuration options have been saved.'));
}
